/*
 * Copyright (c) 2008, 2012 Oracle and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package bu;

import fev1.Element;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.stage.Stage;

/**
 * A sample that demonstrates various mouse and scroll events and their usage.
 * Click the circles and drag them across the screen. Scroll the whole screen.
 * All events are logged to the console.
 *
 * @see javafx.scene.Cursor
 * @see javafx.scene.input.MouseEvent
 * @see javafx.event.EventHandler
 */
public class MouseEvents extends Application {
	//create a console for logging mouse events
	final static ListView<String> console = new ListView<String>();                                /// kle
	//create a observableArrayList of logged events that will be listed in console
	final static ObservableList<String> consoleObservableList = FXCollections.observableArrayList();
	
	BorderPane borderpane = new BorderPane();
	
	MenuBar menuBar = new MenuBar();
	Pane circuitPane = new Pane();
	VBox lfmenu = new VBox();
	
	
	{
		//set up the console
		console.setItems(consoleObservableList);
		console.setLayoutY(305);
		console.setPrefSize(500, 195);
	}
	//create a rectangle - (500px X 300px) in which our circles can move
	


	

	//variables for storing initial position before drag of circle
	private double initX;
	private double initY;
	private Point2D dragAnchor;

	private void init(Stage primaryStage) {



	  
		
		primaryStage.setScene(new Scene(borderpane, 800,600)); 
		


		primaryStage.setResizable(true);

	

		Menu menuFile = new Menu("File");
		Menu menuEdit = new Menu("Edit");
		Menu menuView = new Menu("View");

		menuBar.getMenus().addAll(menuFile, menuEdit, menuView);


		//Create and add the "File" sub-menu options. 

		MenuItem openFile = new MenuItem("Open File");
		MenuItem saveFile = new MenuItem("Save File");
		MenuItem exitApp = new MenuItem("Exit");
		menuFile.getItems().addAll(openFile,saveFile,exitApp);



		
		Button bt1 = new Button();
		bt1.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//crate logical dor
				
				
				//PorteLogique porte = new PorteLogique("nouvelle_portr",circuitPane);
				//circuitPane.getChildren().add(porte);
				showOnConsole("Un nouveau Cercle ajoutee");
			}
		});

		
		
		//lfmenu.set

		borderpane.setTop(menuBar);
		borderpane.setCenter(circuitPane);
		borderpane.setLeft(lfmenu);
		borderpane.setBottom(console);

		circuitPane.setStyle("-fx-background-color: white;");
		bt1.setText("make club Sandwich");
		
		
		
		
		lfmenu.getChildren().add(bt1);


		
		
		
		
		
		
		//circleBig.setTranslateX(300);
		//circleBig.setTranslateY(150);
		//Rectangle node2 = createRectangle("and");

		// we can set mouse event to any node, also on the rectangle
		
		// bizzar mais non car contien root
		borderpane.getCenter().setOnMouseMoved(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//log mouse move to console, method listed below
				showOnConsole("Mouse moved, x: " + me.getX() + ", y: " + me.getY() );
			}
		});

		// show all the circle , rectangle and console
		circuitPane.getChildren().addAll();

	}




	public static void showOnConsole(String text) {
		//if there is 8 items in list, delete first log message, shift other logs and  add a new one to end position
		if (consoleObservableList.size()==8) {
			consoleObservableList.remove(0);
		}
		consoleObservableList.add(text);
	}

	@Override public void start(Stage primaryStage) throws Exception {
		init(primaryStage);
		primaryStage.show();
	}

	
	public static void main(String[] args) {
		launch(args);
	}
}
