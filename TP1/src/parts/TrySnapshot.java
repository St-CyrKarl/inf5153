package parts;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
		
public class TrySnapshot extends Application {

    @Override
    public void start(Stage primaryStage) {
        final VBox vbox = new VBox(2);
        final Button btn = new Button();
        vbox.getChildren().add(btn);
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // here we make image from vbox and add it to scene, can be repeated :)
                WritableImage snapshot = vbox.snapshot(new SnapshotParameters(), null);

                vbox.getChildren().add(new ImageView(snapshot));
                System.out.println(vbox.getChildren().size());
            }
        });

        Scene scene = new Scene(new Group(vbox), 300, 250);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}