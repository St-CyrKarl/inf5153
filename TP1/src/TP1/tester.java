package TP1;





import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.stage.Stage;



public class tester extends Application {



	//create a console for logging mouse events
	final static ListView<String> console = new ListView<String>();                                /// kle
	//create a observableArrayList of logged events that will be listed in console
	final static ObservableList<String> consoleObservableList = FXCollections.observableArrayList();

	BorderPane borderpane = new BorderPane();

	MenuBar menuBar = new MenuBar();
	Menu menuFile = new Menu("Fichier");
	MenuItem openFile = new MenuItem("Ouvrir Circuit");
	MenuItem saveFile = new MenuItem("Sauvegarder Circuit");
	MenuItem exitApp = new MenuItem("Quitter");

	Menu menuEdit = new Menu("Editer");
	Menu menuView = new Menu("Option");

	Pane circuitPane = new Pane();
	
	
	VBox lfmenu = new VBox(); // menu a option pour cree porte suprim porte ...
	Button bt1 = new Button();
	Button ButtonDrawLine = new Button();
	

	{
		//set up the console  pour de debugase
		console.setItems(consoleObservableList);
		console.setLayoutY(305);
		console.setPrefSize(500, 195);
	}


	private void init(Stage primaryStage) {


		primaryStage.setScene(new Scene(borderpane, 800,600)); 
		primaryStage.setResizable(true);


		borderpane.setTop(menuBar);
		borderpane.setCenter(circuitPane);
		circuitPane.setStyle("-fx-background-color: #CCB2FF");
		borderpane.setLeft(lfmenu);


		menuBar.getMenus().addAll(menuFile, menuEdit, menuView);

		menuFile.getItems().addAll(openFile,saveFile,exitApp);




		Button bt1 = new Button();
		Button bt2 = new Button();
		Button bt3 = new Button();
		Button ButtonPorteFichier = new Button();
		bt1.setText("Porte OU");
		bt2.setText("Porte ET");
		bt3.setText("Porte PAS");
		ButtonPorteFichier.setText("Porte fichier");
		
		
		
		
		
		Button ButtonLier = new Button();
		ButtonLier.setText("ajout connexions");
		
		Button ButtonSupprimer = new Button();
		ButtonSupprimer.setText("Supprimer Porte");

		//lfmenu.set

		borderpane.setBottom(console);

		//circuitPane.setStyle("-fx-background-color: white;");
		
		lfmenu.getChildren().addAll(bt1,bt2,bt3,ButtonPorteFichier, ButtonLier,ButtonSupprimer);


		//testing parts

		PorteLogique p1 = new PorteLogique("p1",circuitPane);
		PorteLogique p2 = new PorteLogique("p2",circuitPane);
		PorteLogique p3 = new PorteLogique("p3", circuitPane);
		p2.porteEntre = p1;
		p1.porteSortie = p2;
		p2.porteSortie = p3;
		p3.porteEntre = p2;
		

		




		bt1.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//crate logical dor

				//p2.MAJEntree(circuitPane);
				
				
				new PorteLogique("nouvelle_portr",circuitPane);
				
				showOnConsole("Un nouveau Cercle ajoutee");
			}
		});


		borderpane.getCenter().setOnMouseMoved(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//log mouse move to console, method listed below

				showOnConsole("Mouse moved, x: " + me.getX() + ", y: " + me.getY() );
			}
		});
		///circuitPane.getChildren().addAll(p1,p2);

	}




	public static void showOnConsole(String text) {
		//if there is 8 items in list, delete first log message, shift other logs and  add a new one to end position
		if (consoleObservableList.size()==8) {
			consoleObservableList.remove(0);
		}
		consoleObservableList.add(text);
	}
	@Override public void start(Stage primaryStage) throws Exception {
		init(primaryStage);
		primaryStage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}

}