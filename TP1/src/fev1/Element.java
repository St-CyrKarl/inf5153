package fev1;

import java.awt.Point;

import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.shape.StrokeLineCap;

/**
 *
 * @author antoine
 */
public class Element extends Rectangle{

	Element(Color color, DoubleProperty x, DoubleProperty y,Pane paneParent) {
		super(0, 0, x.get(), y.get());
		//this.setFill(new ImagePattern(new Image("imageedit_1_2352046255.gif"), 0, 0, 1, 1, true));
		paneParent.getChildren().add(this);
	}


	private double initX;
	private double initY;

	boolean  tableLogique [][];

	private Point2D dragAnchor;
	Element porteEntre; // liason
	Element porteSortie;
	Line lineO;
	
	Circuit circuit;
	             

	Element(final String name,Circuit paneParent,int nbEntre,int nbSortie,boolean  tL [][]){
		this(name,paneParent);
		tableLogique = new boolean [nbEntre][nbSortie];
		this.tableLogique = tL;
	}
	
	

	Element(final String name,Circuit circuit) {
		//create a circle with desired name,  color and radius
		

		super(0, 0, 100, 100);
		this.circuit = circuit;
		this.setFill(new ImagePattern(new Image("imageedit_1_2352046255.gif"), 0, 0, 1, 1, true));
		//paneParent.getChildren().add(this);

		setCursor(Cursor.HAND);
		//add a mouse listeners
		setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				Editeur.showOnConsole("Clicked on" + name + ", " + me.getClickCount() + "times");
				//the event will be passed only to the circle which is on front
				me.consume();
			}
		});
		setOnMouseDragged(new EventHandler<MouseEvent>() {


			public void handle(MouseEvent me) {
				double dragX = me.getSceneX() - dragAnchor.getX();
				double dragY = me.getSceneY() - dragAnchor.getY();
				//calculate new position of the circle
				double newXPosition = initX + dragX;
				double newYPosition = initY + dragY;
				
				
				// pour ne pas perrmettre de sortir du circuit a mettre dans editeur
				
				if ((newXPosition>=0) && (newXPosition<=circuit.getWidth())) {
					setTranslateX(newXPosition);
				}
				if ((newYPosition>=0) && (newYPosition<=circuit.getHeight())){
					setTranslateY(newYPosition);
				}         
				
				if(porteSortie != null){
					circuit.getChildren().remove(lineO);
					MAJSortie(circuit);
				}
				if(porteEntre != null){
					circuit.getChildren().remove(porteEntre.lineO);
					porteEntre.MAJSortie(circuit);
				}
			}
		});
		
		setOnMouseEntered(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//change the z-coordinate of the circle
				setStroke(Color.BLACK);
				toFront();
				Editeur.showOnConsole("Mouse entered " + name);
			}
		});
		setOnMouseExited(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				setStroke(null);
				Editeur.showOnConsole("Mouse exited " +name);
			}
		});
		setOnMousePressed(new EventHandler<MouseEvent>() {
		

			public void handle(MouseEvent me) {
				circuit.setSelection(getSelf());
				//when mouse is pressed, store initial position
				initX = getTranslateX();
				initY = getTranslateY();
				dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
				
				
				
				Editeur.showOnConsole("Mouse pressed above " + name);
				

			}
		});
		this.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				Editeur.showOnConsole("Mouse released above " +name);
			}
		});

	}
	Element(final String name,Circuit circuit,String path){
		this(name, circuit);
		this.setFill(new ImagePattern(new Image(path), 0, 0, 1, 1, true));
		
	}
	
	public Element getSelf(){
		return this;
	}

	public boolean MAJSortie(Pane paneParent){
		boolean MAJ = false;

		if(this.porteSortie == null){
			return MAJ;
		}else{
			MAJ = true;

			lineO = new Line();

			lineO.setStartX(this.getTranslateX()+90);
			lineO.setStartY(this.getTranslateY()+50);
			lineO.setEndX(this.porteSortie.getTranslateX()+10);
			lineO.setEndY(this.porteSortie.getTranslateY()+40);

			paneParent.getChildren().add(lineO);
			lineO.toFront();

			return MAJ;
		}
	} 
	public void supprime (){
		if(porteEntre != null){
			circuit.getChildren().remove(this.porteEntre.lineO);
			this.porteEntre.porteSortie = null;
			this.porteEntre = null;
		}
		if(porteSortie != null){
			circuit.getChildren().remove(lineO);
			porteSortie.porteEntre = null;
			porteEntre = null;
		}
		circuit.getChildren().remove(this);
	}

	class BoundLine extends Line {
		BoundLine(DoubleProperty startX, DoubleProperty startY, DoubleProperty endX, DoubleProperty endY) {
			startXProperty().bind(startX);
			startYProperty().bind(startY);
			endXProperty().bind(endX);
			endYProperty().bind(endY);
			setStrokeWidth(2);
			setStroke(Color.GRAY.deriveColor(0, 1, 1, 0.5));
			setStrokeLineCap(StrokeLineCap.BUTT);
			getStrokeDashArray().setAll(10.0, 5.0);
			setMouseTransparent(true);
		}
	}
}
