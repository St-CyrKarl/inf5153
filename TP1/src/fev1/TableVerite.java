package fev1;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
 

//http://docs.oracle.com/javafx/2/ui_controls/table-view.htm
public class TableVerite extends Stage {
 
    private TableView table;
    private Scene scene;
    private TableColumn firstNameCol;
    private TableColumn lastNameCol;
    private TableColumn emailCol;
    VBox vbox;
    
    public TableVerite(){
    	
    	
    	
    	table = new TableView();
    	scene = new Scene(new Group());
        setTitle("Table de verite");
        setWidth(300);
        setHeight(500);
 
        final Button buttonCalculer = new Button("calculer");
        final Label label = new Label("Table de verite");
        label.setFont(new Font("Arial", 20));
 
        table.setEditable(false);
 
        firstNameCol = new TableColumn("ENTR1");
        lastNameCol = new TableColumn("ENTR2");
        emailCol = new TableColumn("SRT1");
        
        table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);
 
        vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label,buttonCalculer, table);
 
        ((Group) scene.getRoot()).getChildren().addAll(vbox);
 
        setScene(scene);
        show();
    	
    }
    public void initTable(){
	
    }
 
}