package fev1;




import parts.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class Editeur extends Application {
	//create a console for logging mouse events
	final static ListView<String> console = new ListView<String>();                                /// kle
	//create a observableArrayList of logged events that will be listed in console
	final static ObservableList<String> consoleObservableList = FXCollections.observableArrayList();

	private boolean dragingMode = true;
	
	
	private BorderPane borderpane = new BorderPane();

	private MenuBar menuBar = new MenuBar();
	private Menu menuFile = new Menu("Fichier");
	private MenuItem newFile = new MenuItem("Nouveau Circuit");
	private MenuItem openFile = new MenuItem("Charger Circuit");
	private MenuItem saveAsXMI = new MenuItem("Enregistrer Circuit");
	private MenuItem saveAsPNG = new MenuItem("Enregistrer Image");
	private MenuItem exitApp = new MenuItem("Quitter");

	private Menu menuEdit = new Menu("Editer");
	private MenuItem undo = new MenuItem("retour arrieur");
	private MenuItem redo = new MenuItem("retour avant");
	
	private Menu menuView = new Menu("Option");

	private Circuit circuitPane = new Circuit();
	
	
	private VBox lfmenu = new VBox(); // menu a option pour cree porte suprim porte ...
	private Button bt1 = new Button();
	private Button ButtonDrawLine = new Button();
	
	
	private Element selection;
	

	{
		//set up the console  pour de debugase
		console.setItems(consoleObservableList);
		console.setLayoutY(305);
		console.setPrefSize(500, 195);
	}


	private void init(Stage primaryStage) {
		
		

		primaryStage.setScene(new Scene(borderpane, 800,600)); 
		primaryStage.setResizable(true);
		
		TableVerite StageTableVerite = new TableVerite(); 
		//StageTableVerite.setScene(new Scene(new Group(new Text(10,10, "my second window")))); 
		//StageTableVerite.show();

		borderpane.setTop(menuBar);
		borderpane.setCenter(circuitPane);
		circuitPane.setStyle("-fx-background-color: #CCB2FF");
		borderpane.setLeft(lfmenu);
		
		


		menuBar.getMenus().addAll(menuFile, menuEdit, menuView);
		menuFile.getItems().addAll(newFile,openFile,saveAsXMI,saveAsPNG,exitApp);
		menuEdit.getItems().addAll(undo,redo);



		Button bt1 = new Button("Porte Ou");
		bt1.setMaxWidth(Double.MAX_VALUE);
		Button bt2 = new Button("Porte ET");
		bt2.setMaxWidth(Double.MAX_VALUE);
		Button bt3 = new Button("Porte PAS");
		bt3.setMaxWidth(Double.MAX_VALUE);
		Button ButtonPorteFichier = new Button("Porte fichier");
		ButtonPorteFichier.setMaxWidth(Double.MAX_VALUE);
		Button ButtonSupprimer = new Button("Supprimer Porte");
		ButtonSupprimer.setMaxWidth(Double.MAX_VALUE);
		
		Button ButtonLier = new Button("ajout connexions");
		ButtonLier.setMaxWidth(Double.MAX_VALUE);
		Button ButtonSupLier = new Button("suprimer connexions");
		ButtonSupLier.setMaxWidth(Double.MAX_VALUE);
		
		Button buttonAjoutEntre = new Button("Ajout entre circuit");
		buttonAjoutEntre.setMaxWidth(Double.MAX_VALUE);
		Button buttonAjoutSortie = new Button("Ajout Sortie circuit");
		buttonAjoutSortie.setMaxWidth(Double.MAX_VALUE);
		lfmenu.getChildren().addAll(bt1,bt2,bt3,ButtonPorteFichier,ButtonSupprimer ,ButtonLier,ButtonSupLier,buttonAjoutEntre ,buttonAjoutSortie);
		
		

		borderpane.setBottom(console);

		//bt3.setStyle(".button {-fx-padding: 10;-fx-background-color: #CCFF99;}");
		
		lfmenu.getStylesheets().add("editeur.css"); 


		//testing parts
		Element s1 = new Element("s1",circuitPane,"sortiC.gif");
		
		Element e1 = new Element("p1",circuitPane, "entreC.gif");

		Element p1 = new Element("p1",circuitPane);
		Element p2 = new Element("p2",circuitPane);
		Element p3 = new Element("p3", circuitPane);
		
		e1.porteSortie = p1;
		p1.porteEntre = e1;
		
		p2.porteEntre = p1;
		p1.porteSortie = p2;
		p2.porteSortie = p3;
		p3.porteEntre = p2;
		
		p3.porteSortie = s1;
		s1.porteEntre = p3;
		
		circuitPane.getChildren().addAll(s1,e1,p1,p2,p3);

		////////////////////////////////////////////
	
		borderpane.getCenter().setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				if(dragingMode == true){
					showOnConsole("zzzz");
				}
			}
		});
		

		ButtonSupprimer.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				getEditeur().circuitPane.getSelection().supprime();
				showOnConsole("suprimmmmememem");
			}
		});

		bt1.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//crate logical dor

				//p2.MAJEntree(circuitPane);
				
				
				Element n = new Element("nouvelle_portr",circuitPane);
				circuitPane.getChildren().addAll(n);
				
				showOnConsole("Un nouveau Cercle ajoutee");
			}
		});


		borderpane.getCenter().setOnMouseMoved(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				//log mouse move to console, method listed below

				showOnConsole("Mouse moved, x: " + me.getX() + ", y: " + me.getY() );
			}
		});
		///circuitPane.getChildren().addAll(p1,p2);

	}

	public Editeur getEditeur(){
		return this;
	}


	public static void showOnConsole(String text) {
		//if there is 8 items in list, delete first log message, shift other logs and  add a new one to end position
		if (consoleObservableList.size()==8) {
			consoleObservableList.remove(0);
		}
		consoleObservableList.add(text);
	}
	@Override public void start(Stage primaryStage) throws Exception {
		init(primaryStage);
		primaryStage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}

}